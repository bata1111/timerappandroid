package com.example.bata.testgitlab;

import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       textView=findViewById(R.id.textbbbb);
        CountDownTimer countDownTimer = new CountDownTimer(60 * 1000, 1000) {
            public void onTick(long millisUntilFinished) {
                textView.setText("Seconds remaining: " + millisUntilFinished / 1000);
            }
            public void onFinish() {
                textView.setText("Done !");
            }
        };

        countDownTimer.start();
    }
}
